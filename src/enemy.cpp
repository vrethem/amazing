#include "enemy.h"

int generateRandom(int max)
{
	int randomNumber = rand();
	float random = (randomNumber % max) + 1;
	return (int)random;
}


Enemy::Enemy()
{
  rect.setSize(sf::Vector2f(X_BOX,Y_BOX));
  rect.setPosition(200,200);
  rect.setFillColor(sf::Color::Green);
  sprite.setTextureRect(sf::IntRect(0, 0,32,32));
  sprite.setPosition(400,200);
}

void Enemy::update()
{
  sprite.setPosition(rect.getPosition().x - (32 - X_BOX)/2, rect.getPosition().y - (32 - Y_BOX)/2);
}
#include <iostream>
void Enemy::updateMovement()
{
  if (dirr_counter <= 0 && wait_counter <= 0) 
  {
    dirr_counter = 25;
    wait_counter = 50;
    dir.first = generateRandom(4);
  }
  else if (dirr_counter > 0)
  {
    if (dir.first == 1)
    {
      rect.move(playerSpeed, 0);
      sprite.setTextureRect(sf::IntRect(96 + walk_count*32,2*32,32,32));
    }
    else if (dir.first == 2)
    {
      rect.move(-playerSpeed, 0);
      sprite.setTextureRect(sf::IntRect(96 + walk_count*32,1*32,32,32));
    }
    else if (dir.first == 3)
    {
      rect.move(0, playerSpeed);
      sprite.setTextureRect(sf::IntRect(96 + walk_count*32,0*32,32,32));
    }
    else if (dir.first == 4)
    {
      rect.move(0, -playerSpeed);
      sprite.setTextureRect(sf::IntRect(96 + walk_count*32,3*32,32,32));
    }
    dirr_counter--;
	walk_count++;
    if (walk_count == 3) walk_count = 0;
  }
  else 
  {
    wait_counter--;
	dir.second = true;
	
  }
  
}

