#ifndef ENEMY_H
#define ENEMY_H
#include <SFML/Graphics.hpp>

#include "entity.h"


#define X_BOX 18
#define Y_BOX 28

class Enemy : public Entity
{
public:
  Enemy();
  void update();
  void updateMovement();

 
  int attackDamage{2};
  int hp{15};
private:
  float playerSpeed{1.00};
  int dirr_counter{};
  int walk_count{};
  int wait_counter{};
};
int generateRandom(int);
#endif
