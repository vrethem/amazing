#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>
typedef std::pair<int, bool> dir_bool;



class Entity
{
public:
//Physical object

  sf::RectangleShape rect;
  sf::Sprite sprite;
  sf::Text text;
  
  bool active{true};
  bool destroy{false};
  int x{};
  int y{};
  dir_bool dir{3, true};
private:
 
};




#endif
