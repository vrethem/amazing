#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>

#include "entity.h"
#include "player.h"
#include "projectile.h"
#include "enemy.h"
#include "textDisplay.h"
#include "wall.h"
#include "perk.h"
#include "junction.h"

using namespace std;

#include <fstream>
#include <string>

Junction makeJunction(vector<vector<char> > binaryMap, int j, int i)
{
   Junction junction;


   if (binaryMap[i][j] == '0')
   {
      if (binaryMap[i - 1][j] == '0' || binaryMap[i - 1][j] == 'S' || binaryMap[i - 1][j] == 'E') {
         junction.opertunities++;
         junction.north = true;
         junction.dir_arr[1] = 0;
      }
      if (binaryMap[i + 1][j] == '0' || binaryMap[i + 1][j] == 'S' || binaryMap[i + 1][j] == 'E') {
         junction.opertunities++;
         junction.south = true;
         junction.dir_arr[3] = 0;
      }
      if (binaryMap[i][j - 1] == '0' || binaryMap[i][j - 1] == 'S' || binaryMap[i][j - 1] == 'E') {
         junction.opertunities++;
         junction.west = true;
         junction.dir_arr[2] = 0;

      }
      if (binaryMap[i][j + 1] == '0' || binaryMap[i][j + 1] == 'S' || binaryMap[i][j + 1] == 'S') {
         junction.opertunities++;
         junction.east = true;
         junction.dir_arr[0] = 0;
      }
   }
   return junction;
}


int main()
{
   //Time variables
   sf::Clock clock1;
   sf::Clock clock2;
   //sf::Clock clock3;

   //Variables
   int counter{};
   int counter2{};
   int counter3{};

   sf::RenderWindow window{ sf::VideoMode(32 * 19,32 * 17), "SFML/Game" };

   window.setPosition(sf::Vector2i(800, 100));
   window.setFramerateLimit(60);

   sf::Texture texturePlayer;
   if (!texturePlayer.loadFromFile("texture/linkEdit.png"))
      cerr << "Failed to open texture/linkEdit.png \n";
   sf::Sprite spritePlayer(texturePlayer);
   spritePlayer.setPosition(window.getSize().x / 2, window.getSize().y / 2);
   spritePlayer.setTextureRect(sf::IntRect(0, 0, 27.7, 30));

   sf::Texture textureEnemy;
   if (!textureEnemy.loadFromFile("texture/EnemySpriteSheet.png"))
      cerr << "Failed to open EnemySPriteSheet.png \n";

   sf::Font font;
   if (!font.loadFromFile("texture/Blox2.ttf")) cerr << "Could not load Blox2.tff\n";
   sf::Font font2;
   if (!font2.loadFromFile("texture/8bit.ttf")) cerr << "Could not load 8bit.tff\n";
   sf::Font font3;
   if (!font3.loadFromFile("texture/Arcade.ttf")) cerr << "Could not load Arcade.tff\n";

   sf::Text text("Gold: ", font2, 25);
   text.setColor(sf::Color::Yellow);
   text.setCharacterSize(20);
   text.setPosition(1, 1);

   //Class object
   Player player1;
   player1.sprite.setTexture(texturePlayer);

   //Projectile array 
   vector<Projectile> vect_projectiles;
   //Projectile  Object
   Projectile projectile1;
   projectile1.sprite.setTexture(textureEnemy);
   projectile1.sprite.setTextureRect(sf::IntRect(32 * 11, 32 * 1, 16, 16));

   //Enemy array
   vector<Enemy> vect_enemy;
   //Enemy object
   Enemy enemy1;
   enemy1.sprite.setTexture(textureEnemy);
   enemy1.sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));

   //Text vector 
   vector<textDisplay> vect_text;

   // Text object
   textDisplay textdisplay1;
   textdisplay1.text.setFont(font2);
   textdisplay1.text.setString("Welcome");
   textdisplay1.text.setCharacterSize(12);
   textdisplay1.rect.setPosition(400, 500);
   vect_text.push_back(textdisplay1);

   // Perks vector array
   vector<Perk> vect_perks;
   Perk perk1;
   perk1.sprite.setTexture(textureEnemy);
   perk1.sprite.setTextureRect(sf::IntRect(32 * 10, 0, 32, 32));

   //Wall vector

   ifstream in_file{ "maze_map/map1.txt" };
   if (!in_file.is_open()) cerr << "Could not open map1.txt" << endl;
   int width, height;
   in_file >> height >> width;
   vector<Wall> vect_wall;
   Wall wall;
   wall.sprite.setTexture(textureEnemy);
   wall.sprite.setTextureRect(sf::IntRect(32 * 11, 0, 32, 32));

   // Get map and set sprites
   counter = 0;
   vector<vector<char> > binaryMap;
   binaryMap.clear();
   //binaryMap.resize(height+1);
   vector<char> binaryRow;
   for (int i{ 1 }; i < height + 1; i++)
   {
      string s;
      in_file >> s;
      int j{};


      binaryRow.clear();
      for (char const & c : s)
      {
         binaryRow.push_back(c);

         if (c == '1')
         {
            wall.rect.setPosition(j * 32, i * 32);
            wall.sprite.setPosition(j * 32, i * 32);
            vect_wall.push_back(wall);
         }
         else if (c == 'E')
         {
            perk1.rect.setPosition((j * 32), i * 32);
            perk1.type = END;
            vect_perks.push_back(perk1);

         }
         else if (counter % 4 == 3)
         {
            enemy1.rect.setPosition(j * 32, i * 32);
            vect_enemy.push_back(enemy1);
         }
         else if (counter % 12 == 1)
         {
            perk1.rect.setPosition((j * 32), i * 32);
            perk1.type = COIN;
            vect_perks.push_back(perk1);
         }
         if (c == 'S') player1.rect.setPosition(j * 32, i * 32);
         j++;
         counter++;
         cout << c;
      }
      cout << endl;
      binaryMap.push_back(binaryRow);
   }
   in_file.close();


   cout << endl << "Reading maze to vector: " << endl;
   for (int i{ 1 }; i < (int)binaryMap.size() - 1; i++)
   {
      for (int j{ 1 }; j < (int)binaryMap.at(i).size() - 1; j++)
      {
         Junction junction;
         cout << binaryMap[i][j];

         // Normal junction point 
         if (binaryMap[i][j] == '0')
         {
            if (binaryMap[i - 1][j] == '0' || binaryMap[i - 1][j] == 'S' || binaryMap[i - 1][j] == 'E') {
               junction.opertunities++;
               junction.north = true;
               junction.dir_arr[1] = 0;
               if (binaryMap[i + 1][j] == '0' || binaryMap[i + 1][j] == 'S' || binaryMap[i + 1][j] == 'E') {
                  junction.south = true;
                  junction.dir_arr[3] = 0;
               }
            }
            else if (binaryMap[i + 1][j] == '0' || binaryMap[i + 1][j] == 'S' || binaryMap[i + 1][j] == 'E') {
               junction.opertunities++;
               junction.south = true;
               junction.dir_arr[3] = 0;
            }
            if (binaryMap[i][j - 1] == '0' || binaryMap[i][j - 1] == 'S' || binaryMap[i][j - 1] == 'E') {
               junction.opertunities++;
               junction.west = true;
               junction.dir_arr[2] = 0;
               if (binaryMap[i][j + 1] == '0' || binaryMap[i][j + 1] == 'S' || binaryMap[i][j + 1] == 'E') {
                  junction.east = true;
                  junction.dir_arr[0] = 0;
               }
            }
            else if (binaryMap[i][j + 1] == '0' || binaryMap[i][j + 1] == 'S' || binaryMap[i][j + 1] == 'S') {
               junction.opertunities++;
               junction.east = true;
               junction.dir_arr[0] = 0;
            }
         }
         if (junction.opertunities > 1) //L�gg till vid varje v�gkors
         {
            junction.rect.setPosition(j * 32, i * 32 + 32);
            player1.vect_junc.push_back(junction);
         }
			
         //Set junction point just beside normal junction points
         /*junction = makeJunction(binaryMap, j, i);
           if (junction.opertunities > 1) //L�gg till vid varje v�gkors
           {
           if (junction.east) {
           junction = makeJunction(binaryMap, j + 1, i);
           junction.rect.setPosition(j * 32 + 32, i * 32 + 32);
           if (junction.opertunities > 1) //L�gg till vid varje v�gkors
           player1.vect_junc.push_back(junction);
           }
           if (junction.north) {
           junction = makeJunction(binaryMap, j, i + 1);
           junction.rect.setPosition(j * 32, i * 32 + 32 + 32);
           if (junction.opertunities > 1) //L�gg till vid varje v�gkors
           player1.vect_junc.push_back(junction);
           }
           if (junction.west) {
           junction = makeJunction(binaryMap, j - 1, i);
           junction.rect.setPosition(j * 32 - 32, i * 32 + 32);
           if (junction.opertunities > 1) //L�gg till vid varje v�gkors
           player1.vect_junc.push_back(junction);
           }
           if (junction.south) {
           junction = makeJunction(binaryMap, j, i - 1);
           junction.rect.setPosition(j * 32, i * 32 );
           if (junction.opertunities > 1) //L�gg till vid varje v�gkors
           player1.vect_junc.push_back(junction);
           }
           }*/


      }
      cout << endl;
   }


   static bool game_over{ false };
   //Start game loop
   while (window.isOpen() && !game_over)
   {
      sf::Event event;
      while (window.pollEvent(event))
      {
         if (event.type == sf::Event::Closed) window.close();
         if (event.type == sf::Event::KeyPressed) {
            if (event.key.code == sf::Keyboard::Escape) window.close();
         }

      }
      //Clear screen
      window.clear(sf::Color(200, 140, 140));


      sf::Time elapsed1 = clock1.getElapsedTime();
      sf::Time elapsed2 = clock2.getElapsedTime();
      //	sf::Time elapsed3 = clock3.getElapsedTime();

      //////////////////////// Input //////////////////////////////////////
      {
         //Update player
         {

            player1.update();
            Player tmp{ player1 };
            player1.AIwithJunctionMovement();


            // Player collide with wall
            bool collision{ false };
            for (auto it = vect_wall.begin(); it != vect_wall.end(); it++)
            {
               if (player1.rect.getGlobalBounds().intersects(it->rect.getGlobalBounds()))
               {
                  collision = true;
               }
            }
            if (collision)
            {
               player1.rect.setPosition(tmp.rect.getPosition());
            }
         }

         //Fire missile 
         if (elapsed1.asSeconds() >= 0.3 && player1.fire)
         {
            clock1.restart();

            projectile1.rect.setPosition(player1.rect.getPosition().x +
                                         projectile1.rect.getSize().x / 2,
                                         player1.rect.getPosition().y +
                                         projectile1.rect.getSize().y / 2);

            projectile1.dir = player1.dir;
            vect_projectiles.push_back(projectile1);
            //cout << "FIRE!!" << endl;
            player1.fire = false;
         }
      }


      ///////////////////////// Update ////////////////////////////////////
      {
         // Enemy collide with player 
         if (elapsed2.asSeconds() >= 0.2)
         {
            clock2.restart();
            counter = 0;
            for (auto it = vect_enemy.begin(); it != vect_enemy.end(); it++)
            {
               if (player1.rect.getGlobalBounds().intersects(vect_enemy.at(counter).rect.getGlobalBounds()))
               {
                  // Text display
                  textdisplay1.text.setString(to_string(vect_enemy.at(counter).attackDamage));
                  textdisplay1.text.setPosition(player1.rect.getPosition().x + player1.rect.getSize().x / 2
                                                , player1.rect.getPosition().y - player1.rect.getSize().y / 2);
                  textdisplay1.text.setColor(sf::Color::Red);
                  vect_text.push_back(textdisplay1);

                  player1.hp -= vect_enemy.at(counter).attackDamage;
               }
               counter++;
            }
         }

         //Projectile collide with enemy
         counter = 0;
         for (auto it = vect_projectiles.begin(); it != vect_projectiles.end(); it++)
         {
            counter2 = 0;
            for (auto it = vect_enemy.begin(); it != vect_enemy.end(); it++)
            {
               //Enemy hit
               if (vect_projectiles.at(counter).rect.getGlobalBounds().intersects(vect_enemy.at(counter2).rect.getGlobalBounds()))
               {
                  // Text display
                  textdisplay1.text.setString(to_string(vect_projectiles.at(counter).damage));
                  textdisplay1.text.setPosition(vect_enemy.at(counter2).rect.getPosition().x + vect_enemy.at(counter2).rect.getSize().x / 2
                                                , vect_enemy.at(counter2).rect.getPosition().y - vect_enemy.at(counter2).rect.getSize().y / 2);
                  textdisplay1.text.setColor(sf::Color::Blue);
                  vect_text.push_back(textdisplay1);

                  vect_enemy.at(counter2).hp -= vect_projectiles.at(counter).damage;
                  vect_projectiles.at(counter).destroy = true;
                  // Enemy deadss
                  if (vect_enemy.at(counter2).hp <= 0)
                  {
                     vect_enemy.at(counter2).destroy = true;

                     if (generateRandom(2) == 2)
                     {
                        perk1.type = COIN;
                        perk1.rect.setPosition(vect_enemy.at(counter2).rect.getPosition());
                        vect_perks.push_back(perk1);
                     }
                  }
               }
               counter2++;
            }
            counter++;
         }

         //Pickup perks	
         for (auto it = vect_perks.begin(); it != vect_perks.end(); it++)
         {
            if (player1.rect.getGlobalBounds().intersects(it->rect.getGlobalBounds()))
            {
               switch (it->type)
               {
               case END:
                  game_over = true;
                  break;
               case COIN:
                  player1.gold += it->coinValue;
                  break;
               default:
                  break;
               }
               vect_perks.erase(it);
               break;
            }
         }

         //Projectile collision
         for (auto it = vect_projectiles.begin(); it != vect_projectiles.end(); it++)
         {
            // Frame box for projectiles
            if (it->rect.getPosition().x < 0 || it->rect.getPosition().y < 0 ||
                it->rect.getPosition().x + it->rect.getLocalBounds().width >= window.getSize().x ||
                it->rect.getPosition().y + it->rect.getLocalBounds().height >= window.getSize().y)
            {
               it->destroy = true;
            }
            // Projectile collide with wall
            for (auto it2 = vect_wall.begin(); it2 != vect_wall.end(); it2++)
            {
               if (it2->rect.getGlobalBounds().intersects(it->rect.getGlobalBounds()))
                  it->destroy = true;
            }
            //Delete dead projectiles
            if (it->destroy)
            {
               //cout << "Projectile hit" << endl;
               vect_projectiles.erase(it);
               break;
            }
         }

         //Enemy collision
         for (auto it = vect_enemy.begin(); it != vect_enemy.end(); it++)
         {
            //Delete dead enemies
            if (it->destroy)
            {
               cout << "Enemy dead" << endl;
               vect_enemy.erase(it);
               break;
            }
            // Enemy collide with wall
            auto tmp = it->rect.getPosition();
            it->updateMovement();
            bool collision{ false };
            for (auto it2 = vect_wall.begin(); it2 != vect_wall.end(); it2++)
            {
               if (it->rect.getGlobalBounds().intersects(it2->rect.getGlobalBounds()))
                  it->rect.setPosition(tmp);
            }
         }

         // Text update
         for (auto it = vect_text.begin(); it != vect_text.end(); it++)
         {
            it->update();
            if (it->destroy)
            {
               vect_text.erase(it);
               break;
            }
         }
      }

      /////////////////////////// Draw ////////////////////////////////////
      {
         {
            for (auto it = player1.vect_junc.begin(); it != player1.vect_junc.end(); it++)
            {
               window.draw(it->rect);
            }

            // Draw wall
            counter = 0;
            for (auto it = vect_wall.begin(); it != vect_wall.end(); it++)
            {
               window.draw(vect_wall.at(counter).sprite);
               counter++;
            }

            //Draw Perks	
            counter = 0;
            for (auto it = vect_perks.begin(); it != vect_perks.end(); it++)
            {
               vect_perks.at(counter).update();
               window.draw(vect_perks.at(counter).sprite);
               window.draw(vect_perks.at(counter).rect);
               counter++;
            }

            //Draw projectile
            counter = 0;
            for (auto it = vect_projectiles.begin(); it != vect_projectiles.end(); it++)
            {
               vect_projectiles.at(counter).updateMovement();
               window.draw(vect_projectiles.at(counter).rect);
               window.draw(vect_projectiles.at(counter).sprite);
               counter++;
            }

            //Draw enemies
            counter = 0;
            for (auto it = vect_enemy.begin(); it != vect_enemy.end(); it++)
            {
               vect_enemy.at(counter).update();
               window.draw(vect_enemy.at(counter).rect);
               window.draw(vect_enemy.at(counter).sprite);
               counter++;
            }
            //Draw player
            window.draw(player1.rect);
            window.draw(player1.sprite);

            //Draw text
            counter = 0;
            for (auto it = vect_text.begin(); it != vect_text.end(); it++)
            {
               window.draw(vect_text.at(counter).text);
               counter++;
            }
            // Draw Gold
            text.setString("Gold: " + to_string(player1.gold));
            window.draw(text);

         }
      }


      //////////////////////////// Display ////////////////////////////////
      window.display();

   }

}





