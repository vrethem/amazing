#ifndef JUNCTION_H
#define JUNCTION_H

#include "entity.h"


class Junction : public Entity
{
public:
	Junction()
	{
		rect.setSize(sf::Vector2f(32, 32));
		rect.setFillColor(sf::Color::Magenta);
	}
	void update()
	{
		visited++;
		if (dir_arr[0] > 1) east = false;
		if (dir_arr[1] > 1) north = false;
		if (dir_arr[2] > 1) west = false;
		if (dir_arr[3] > 1) south = false;
	}
	bool east{ false };
	bool north{ false };
	bool west{ false };
	bool south{ false };

	int dir_arr[4] = {99, 99, 99, 99 };


	int visited{};
	int opertunities{};
private:

};


#endif