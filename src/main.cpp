#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
//#include "/usr/include/SFML/Graphics/Rect.hpp"
#include <iostream>
#include <string>

#include "map_handling.h"

using namespace std;

#define X_SPEED 3
#define Y_SPEED 3

void go_thru_maze()
{
}


int main()
{
  // Create advanced settings for the window
  sf::ContextSettings settings;
  // Set the antialiasing level ("smoothness" level)
  // Note: might not work on all graphics cards
  settings.antialiasingLevel = 8;
    
  // Create the window
  sf::RenderWindow window {sf::VideoMode {640, 480},
      "amazing", sf::Style::Default, settings};
  window.setKeyRepeatEnabled(false);
//  window.setMouseCursorVisible(false);

  //Load texture
  sf::Texture wall;
  if(!wall.loadFromFile("texture/wall.png", sf::IntRect(0, 0, 32, 32))) return 1;


  sf::Texture dot;
  if(!dot.loadFromFile("texture/dot.png", sf::IntRect(0, 0, 32, 32))) return 1;

  // Create sprite
  sf::Sprite sprite_dot;
  sprite_dot.setTexture(dot);
  sprite_dot.setPosition(35, 35);

  Map map{"maze_map/map1.txt"};

  sf::Clock clock;
  sf::Time targetFrameDelay {sf::milliseconds(100)};

  bool running{true};
  while(running)
  {
    clock.restart();
    int xSpeed{}, ySpeed{};
    
    // Get events
    sf::Event event;
    while( window.pollEvent(event) )
    {//if the window was closed from the outside
      if (event.type == sf::Event::Closed ) running = false;
      else if (event.type == sf::Event::KeyPressed)
      {
        if (event.key.code == sf::Keyboard::Escape) running = false;
      }
    }
 //Get key press
      if ( event.key.code == sf::Keyboard::W )
        ySpeed = -Y_SPEED;       
      else if ( event.key.code == sf::Keyboard::A )
        xSpeed = -X_SPEED;
      else if ( event.key.code == sf::Keyboard::S )
        ySpeed = Y_SPEED;
      else if ( event.key.code == sf::Keyboard::D )
        xSpeed = X_SPEED;  
   
 
    sprite_dot.move(xSpeed, ySpeed);
    
    //clear screen
    window.clear(sf::Color(255,255,255));

    //Draw things
    for( sf::Sprite s: map.v_sprite) {
      s.setTexture(wall);
      window.draw(s);
    }
    
    window.draw(sprite_dot);
    
    // Display 
    window.display();
// wait before drawing the next frame
    auto frameDelay = clock.getElapsedTime();
    if ( targetFrameDelay > frameDelay )
    {
      // only wait if it is actually needed
      auto sleepTime = targetFrameDelay - frameDelay;
      sf::sleep(sleepTime);
    }
  }
}
