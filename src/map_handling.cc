#include "map_handling.h"
#include <iostream>
#include <fstream>

using namespace std;
//Konstruktor
Map::Map(string filename)
{
  vector<vector<pair_type> > map = get_map(filename);
  for (unsigned int y{}; y < map.size(); y++) {
    for (unsigned int x{}; x < map.at(y).size(); x++)
    {
      sf::Sprite sprite;
      if (map[y][x].first == '1')
      {
        sprite.setPosition(32*x, 32*y);
        v_sprite.push_back(sprite);
      }
    }
  }
}

vector<vector<pair_type> > Map::get_map(string filename)
{
  vector<vector<pair_type> > map;
  ifstream in(filename);
  if (!in.is_open() ) throw invalid_argument("Could not open FILE");

  int height, width;
  in >> height >> width;

  for(int i{height-1}; i >= 0; i--) {
    string s;
    in >> s;
    vector<pair_type> v;
    int j{};
    for_each(begin(s), end(s), [&v, &i, &j] (char c) {
        v.push_back(pair_type(c, cords(i,j)));
        j++;
      });
    map.push_back(v);
 } 
  in.close();
  return map;
}

void Map::print(vector<vector<pair_type> > map)
{
  for (vector<pair_type> const & r : map) {
    for (pair_type const & p: r) cout << p.first << ' ';
//cout << '(' << p.second.first <<  ',' << p.second.second << ')';
    cout << endl;
  }
}
