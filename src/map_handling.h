#ifndef MAP_HANDLING_H
#define MAP_HANDLING_H
#include <SFML/Graphics.hpp>
#include <vector>
#include <string>

typedef std::pair<int,int> cords;
typedef std::pair<char, cords> pair_type;


class Map {
public:
  Map(std::string filename);
  std::vector<sf::Sprite> v_sprite;
private:
  std::vector<std::vector<pair_type> >  get_map(std::string filename);
  void print(std::vector<std::vector<pair_type> > map);
private:
  

};

#endif
