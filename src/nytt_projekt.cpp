#include <SFML/Graphics.hpp>
#include <iostream>

using namespace std;

int main()
{
  sf::RenderWindow window(sf::VideoMode(640,480), "SFML/Window", sf::Style::Default ); 
  window.setKeyRepeatEnabled(false);

  sf::Time time = sf::milliseconds(0);
  sf::Time targetFrameDelay {sf::milliseconds(20)};
  sf::Clock clock;
  while(window.isOpen())
  {
    clock.restart();
    sf::Event event;
    while(window.pollEvent(event))
    {
      switch(event.type) {
        case sf::Event::Closed: 
          window.close();
          break;
        case sf::Event::GainedFocus: 
          cout << "Window active \n";
          break;
        case sf::Event::LostFocus:
          cout << "Winow not active\n";
          break;
        default:
          break;
      }
if ( event.key.code == sf::Keyboard::W )
      cout << 'w';       
    else if ( event.key.code == sf::Keyboard::A )
      cout << 'a';
    else if ( event.key.code == sf::Keyboard::S )
      cout << 's';
    else if ( event.key.code == sf::Keyboard::D )
      cout << 'd'; 
    }

     
    // if(window.waitEvent(event))
    time = clock.restart();
    if ( targetFrameDelay > time )
    {
      // only wait if it is actually needed
      auto sleepTime = targetFrameDelay - time;
      sf::sleep(sleepTime);
    }
    window.display();
  }
}
