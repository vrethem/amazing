#include "perk.h"

#include <iostream>
Perk::~Perk()
{
	//std::cout << "Destruktor perk \n";
}


Perk::Perk()
	:type{ COIN }
{
	rect.setSize(sf::Vector2f(SPRITE_SIZE/2, SPRITE_SIZE/2));
	rect.setPosition(0, 0);
	//sprite.setTextureRect(sf::IntRect(SPRITE_SIZE * 1, SPRITE_SIZE * 0, SPRITE_SIZE, SPRITE_SIZE));
}

Perk::Perk(perk_type type)
:type{ type }
{
rect.setSize(sf::Vector2f(SPRITE_SIZE, SPRITE_SIZE));
rect.setPosition(0, 0);
}

void Perk::update()
{
	sprite.setPosition(rect.getPosition().x -4, rect.getPosition().y - 4);
	switch (type)
	{
	case END:
		sprite.setTextureRect(sf::IntRect(SPRITE_SIZE * (9 + sprite_counter), SPRITE_SIZE * 3, SPRITE_SIZE, SPRITE_SIZE));
		if (delay_counter > 10)
		{
			delay_counter = 0;
			sprite_counter++;
			if (sprite_counter == 3) sprite_counter = 0;
		}
		delay_counter++;
		break;
	case COIN:
		sprite.setTextureRect(sf::IntRect(SPRITE_SIZE * (9 + sprite_counter), SPRITE_SIZE * 2, SPRITE_SIZE, SPRITE_SIZE));
		if (delay_counter > 10)
		{
			delay_counter = 0;
			sprite_counter++;
			if (sprite_counter == 3) sprite_counter = 0;
		}
		delay_counter++;
	default:
		//sprite.setTextureRect(sf::IntRect(SPRITE_SIZE * 0, SPRITE_SIZE * 2, SPRITE_SIZE, SPRITE_SIZE));
		break;
	}
}