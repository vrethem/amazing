#ifndef PERK_H
#define PERK_H

#include "entity.h"
#define SPRITE_SIZE 32

enum perk_type { END, START, COIN, BONUS };


class Perk : public Entity
{
public:
	~Perk();
	Perk();
	Perk(perk_type);

	void update();

	perk_type type;
	int coinValue{1};
	int sprite_counter{};
	int delay_counter{};
private:

};



#endif