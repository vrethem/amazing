#include "player.h"
#include <iostream>
using namespace std;
Player::Player()
{
	rect.setSize(sf::Vector2f(X_BOX, Y_BOX));
	rect.setPosition(400, 200);
	rect.setFillColor(sf::Color::Blue);
	sprite.setTextureRect(sf::IntRect(0, 0, 27.7, 30));
	sprite.setPosition(400, 200);
}

void Player::update()
{
	sprite.setPosition(rect.getPosition().x - (27 - X_BOX) / 2, rect.getPosition().y - (27 - Y_BOX) / 2 - 2);
	if (hp <= 0)
	{
		rect.setPosition(128, 5);
	}
}

void Player::updateMovement()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && dir.second)
	{
		dir.first = 1;
		rect.move(playerSpeed, 0);
		sprite.setTextureRect(sf::IntRect(walk_count*27.7, 7 * 30, 27.7, 30));
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && dir.second)
	{
		dir.first = 2;
		rect.move(-playerSpeed, 0);
		sprite.setTextureRect(sf::IntRect(walk_count*27.7, 5 * 30, 27.7, 30));
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && dir.second)
	{
		dir.first = 3;
		rect.move(0, playerSpeed);
		sprite.setTextureRect(sf::IntRect(walk_count*27.7, 4 * 30, 27.7, 30));
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && dir.second)
	{
		dir.first = 4;
		rect.move(0, -playerSpeed);
		sprite.setTextureRect(sf::IntRect(walk_count*27.7, 6 * 30, 27.7, 30));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		fire = true;
	//else sprite.setTextureRect(sf::IntRect(0, 0,27.7,30));
	dir.second = true;
	walk_count++;
	if (walk_count == 9) walk_count = 0;
}

void Player::makeMove()
{	//			 1 EAST  2 NORTH    3 WEST     4 SOUTH
	if (dir.first == 1)
	{
		rect.move(playerSpeed, 0);
		sprite.setTextureRect(sf::IntRect(walk_count*27.7, 7 * 30, 27.7, 30));
	}
	else if (dir.first == 2)
	{
		rect.move(0, -playerSpeed);
		sprite.setTextureRect(sf::IntRect(walk_count*27.7, 6 * 30, 27.7, 30));
	}
	else if (dir.first == 3)
	{
		rect.move(-playerSpeed, 0);
		sprite.setTextureRect(sf::IntRect(walk_count*27.7, 5 * 30, 27.7, 30));
	}
	else if (dir.first == 4)
	{
		rect.move(0, playerSpeed);
		sprite.setTextureRect(sf::IntRect(walk_count*27.7, 4 * 30, 27.7, 30));
	}
}


void Player::AIupdateMovement()
{
	if (just_moved)
	{
		//std::cout << "Clockwise" << std::endl;
		dir.first -= 1;
		if (dir.first < 1) dir.first = 4;
		just_moved = false;
	}
	else if (rect.getPosition() == last_pos)
	{
		dir.first += 1;
		if (dir.first > 4) dir.first = 1;
		//std::cout << "CounterClockwise"<< std::endl;
	}
	else if (decision_counter < 0)
	{
		just_moved = true;
		decision_counter = 3;
	}
	decision_counter--;

	last_pos = rect.getPosition();
	makeMove();
	fire = true;

}




void Player::AIwithJunctionMovement()
{
	for (auto it = vect_junc.begin(); it != vect_junc.end(); it++)
	{
		if (rect.getPosition() == it->rect.getPosition())
		{
			cout << "<<<<<<<<<<<<<<<<<<<<@New Junction>>>>>>>>>>>>>>>>>>>>>>" << endl;
			cout << "Direction: "<<  dir.first << endl;
			// Counter on visited 
			int tmp = dir.first;
			tmp++;
			if (tmp > 4) tmp = 1;
			tmp++;
			if (tmp > 4) tmp = 1;
			it->dir_arr[tmp - 1]++;

			//Update junction	
			it->update();

			cout << endl;
			// If reached dead end or if you are at a three optional way to go delete where you came from 
			if (dead_end || it->east + it->north + it->west + it->south >= 3) {
				cout << "For switch: " <<  dir.first << endl;
				switch (dir.first) {
				case 1: it->west = false;
					cerr << "West deleted" << endl;
					break;
				case 2: it->south = false;
					cerr << "South deleteed \n";
					break;
				case 3: it->east = false;
					cerr << "East deleteed \n";
					break;
				case 4: it->north = false;
					cerr << "North deleteed \n";
					break;
				}
				dead_end = false;
			}

			cout << "East	North	West	South" << endl;
			cout << "  " << it->east << '\t' << it->north << '\t' << it->west << '\t' << it->south << endl;
			cout << " " << it->dir_arr[0] << '\t' << it->dir_arr[1] << '\t' << it->dir_arr[2] << '\t' << it ->dir_arr[3] << endl;

			// find min value and go with that path
			int min{ 10 }, min2{ 10 };
			int counter = 0;
			int index{};
			bool multiple{ false };
			//int before = dir.first;
			pair<int, bool> before(dir.first, false);
			switch (dir.first) {
			case 1: if (it->east) before.second = true;
				break;
			case 2:if (it->north) before.second = true;
				break;
			case 3: if (it->west) before.second = true;
				break;
			case 4: if (it->south) before.second = true;
				break;
			}
			vector<bool> directions{ it->east, it->north, it->west, it->south };
			for (auto it2 = begin(it->dir_arr); it2 != end(it->dir_arr); it2++)
			{
				if (*it2 <= min ) // && directions.at(counter))
				{
					if (min == *it2) {
						min2 = *it2;
						multiple = true;
					}
					else {
						min = *it2;
						index = counter;
					}
				}
				counter++;
			}
			// If found min go with it else get ranbdom 
			if (!multiple) {
				dir.first = index + 1;
				cout << "Found min path at: " << dir.first << endl;
			}
			else {
				cout << "Random direction!" << endl;
				bool stop{ false };
				while (!stop)
				{
					dir.first = generateRandom(4);
					while (true)
					{
						if (dir.first != min || dir.first != min2)
							break;
						dir.first = generateRandom(4);
					}
					switch (dir.first) {
					case 1: if (it->east) stop = true;
						break;
					case 2:if (it->north) stop = true;
						break;
					case 3: if (it->west) stop = true;
						break;
					case 4: if (it->south) stop = true;
						break;
					}
				}
			}
			// Add to the direction heading towards
			it->dir_arr[dir.first - 1]++;

			//Update junction	
			it->update();

			makeMove();
			fire = true;
			return;
		}
		
	}
	//If position not chang => Reached dead end
	if (last_pos == rect.getPosition())
	{
		dir.first++;
		if (dir.first > 4) dir.first = 1;
		dir.first++;
		if (dir.first > 4) dir.first = 1;
		cout << "--- DEAD END! --- " << endl;
		dead_end = true;
	}

	last_pos = rect.getPosition();
	
	makeMove();
	fire = true;
}
