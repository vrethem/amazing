#ifndef PLAYER_H
#define PLAYER_H

#include "entity.h"
#include "enemy.h"
#include "junction.h"

#include <vector>

#define X_BOX 16
#define Y_BOX 20

class Player : public Entity
{
public:
	Player();
	void update();
	void updateMovement();
	void AIupdateMovement();
	void AIwithJunctionMovement();

	int attackDamage{ 1 };
	int hp{ 100 };
	int gold{};
	bool fire{ false };

	std::vector<Junction> vect_junc;
private:
	float playerSpeed{ 4.00 };
	int walk_count{};

	
// AI variables
	sf::Vector2f last_pos{0, 0};
	bool just_moved{ true };
	int decision_counter{ 10 };
	bool dead_end{ true};
	int counter{ 10 };
	void makeMove();
	
};

#endif
