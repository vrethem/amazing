#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "entity.h"

class Projectile : public Entity
{
public:
	~Projectile()
	{
		//std::cout << "Projectile destruktor" << std::endl;
	}
	Projectile()
	{
		rect.setSize(sf::Vector2f(10, 10));
		rect.setPosition(0, 0);
		rect.setFillColor(sf::Color::Blue);
	};
	void updateMovement()
	{
		if (dir.first == 1) rect.move(speed, 0);
		else if (dir.first == 2)rect.move(-speed, 0);
		else if (dir.first == 3)rect.move(0, speed);
		else if (dir.first == 4)rect.move(0, -speed);
		sprite.setPosition(rect.getPosition());
	}
		bool destroy{ false };
	float speed{ 6.10f };
	int count{};
	int damage{15};
private:

};



#endif
