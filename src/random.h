#ifndef RANDOM_H
#define RANDOM_H

#include <iostream>

class Random
{
public:
	Random()
	{}

	int r{};
private:
};

int generateRandom(int max)
{
  int randomNumber = rand();
  float random = (randomNumber % max) + 1;
  return (int)random;
}

#endif
