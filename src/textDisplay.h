#pragma once

#ifndef TEXTDISPLAY_H
#define TEXTDISPLAY_H

#include "entity.h"
#include <string>
class textDisplay :public Entity
{
public:
	textDisplay();
	void update();

	std::string message = "Unintialized";
private:
	double speed{ 1.9 };
	int counter{ 0 };
	int lifetime{ 70 };
};

#endif