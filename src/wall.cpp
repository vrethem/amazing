#include "wall.h"

Wall::Wall()
{
	rect.setSize(sf::Vector2f(32, 32));
	rect.setPosition(100, 100);
	sprite.setPosition(100, 100);
	rect.setFillColor(sf::Color::Yellow);
	//sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
}

Wall::Wall(int x, int y)
{
	rect.setSize(sf::Vector2f(32, 32));
	rect.setPosition(x, y);
	sprite.setPosition(x, y);
	rect.setFillColor(sf::Color::Yellow);
	//sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
}